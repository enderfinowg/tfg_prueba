package com.example.tfg_primera_prueba.ui.RutaDefecto;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class RutaViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public RutaViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is gallery fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}