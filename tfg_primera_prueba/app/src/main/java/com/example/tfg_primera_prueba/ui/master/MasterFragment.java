package com.example.tfg_primera_prueba.ui.master;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.tfg_primera_prueba.R;
import com.example.tfg_primera_prueba.databinding.FragmentMasterBinding;
import com.example.tfg_primera_prueba.ui.db.Db;
import com.example.tfg_primera_prueba.ui.master.masterdelete.ModificarMonumento;

public class MasterFragment extends Fragment {

    private MasterViewModel masterViewModel;
    private FragmentMasterBinding binding;
    TextView txtNombre, txtURL, txtID_delete;
    Button añadir;
    Button elminar;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        masterViewModel =
                new ViewModelProvider(this).get(MasterViewModel.class);


        binding = FragmentMasterBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        txtNombre = root.findViewById(R.id.prueba1);
        txtURL = root.findViewById(R.id.prueba2);

        añadir = (Button) root.findViewById(R.id.button1);
        elminar = (Button) root.findViewById(R.id.delete);


        añadir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long id = Db.getInstance().insertarURL(txtNombre.getText().toString(), txtURL.getText().toString());

                if(id > 0)
                {
                    Toast.makeText(getActivity(), "holis", Toast.LENGTH_LONG).show();
                    limpiar();
                }
                else
                {
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
                }
            }
        });





        elminar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ModificarMonumento.class);
                startActivity(intent);
            }
        });


        return root;
    }

    private void limpiar(){
        txtNombre.setText("");
        txtURL.setText("");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}