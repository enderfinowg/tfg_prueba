package com.example.tfg_primera_prueba.ui.Monumentos;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tfg_primera_prueba.R;
import com.example.tfg_primera_prueba.databinding.FragmentMonumentoBinding;
import com.example.tfg_primera_prueba.ui.adaptadores.ListaURLsAdapter;
import com.example.tfg_primera_prueba.ui.db.Db;
import com.example.tfg_primera_prueba.ui.db.URLClass;

import java.util.ArrayList;

public class MonumentosFragment extends Fragment {

    private MonumentosViewModel monumentosViewModel;
    private FragmentMonumentoBinding binding;
    RecyclerView listaURLs;
    ArrayList<URLClass> listaArrayURLs;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        monumentosViewModel =
                new ViewModelProvider(this).get(MonumentosViewModel.class);


        binding = FragmentMonumentoBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        listaURLs = root.findViewById(R.id.ViElem);
        listaURLs.setLayoutManager(new LinearLayoutManager(getActivity()));

        listaArrayURLs = new ArrayList<>();
        ListaURLsAdapter adapter = new ListaURLsAdapter(getActivity(), Db.getInstance().mostrarURLs());
        listaURLs.setAdapter(adapter);


        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }


}