package com.example.tfg_primera_prueba;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.example.tfg_primera_prueba.ui.db.Db;
import com.example.tfg_primera_prueba.ui.db.MonumentoInfo;

public class VisualizacionMonumentos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int id = getIntent().getExtras().getInt("idMonumento");
        MonumentoInfo info = Db.getInstance().getInformacion(id);

        Log.i("AAAAAAAAAAAAAAAAAA", "" +id);

        Log.i("sasdf", info.getTitulo());

        setContentView(R.layout.activity_visualizacion_monumentos);
    }
}