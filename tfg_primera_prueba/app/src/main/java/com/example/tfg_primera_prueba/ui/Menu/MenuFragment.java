package com.example.tfg_primera_prueba.ui.Menu;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.tfg_primera_prueba.databinding.FragmentCrearRutaBinding;

public class MenuFragment extends Fragment {

    private MenuViewModel menuViewModel;
    private FragmentCrearRutaBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        menuViewModel =
                new ViewModelProvider(this).get(MenuViewModel.class);

        binding = FragmentCrearRutaBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        getActivity().setTheme(android.R.style.Theme_Black);

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}