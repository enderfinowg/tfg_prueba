package com.example.tfg_primera_prueba.ui.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class Db
{
    private static Db instance;
    DbHelper dbHelper;
    SQLiteDatabase db;

    public Db (@Nullable Context context )
    {
        dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
        instance = this;
    }

    public static Db getInstance()
    {
        if(instance == null)
        {
            Log.e("DB", "Bruh");
        }
        return instance;
    }

    public SQLiteDatabase getDataBase()
    {
        return db;
    }

    public long insertarURL(String titulo, String url)
    {
        long id = 0;
        try
        {
            ContentValues values = new ContentValues();
            values.put("titulo", titulo);
            values.put("url", url);

            id = db.insert(DbHelper.TABLE_URL, null, values);
        } catch (Exception ex)
        {
            ex.toString();
        }
        return id;
    }

    public ArrayList<URLClass> mostrarURLs()
    {
        ArrayList <URLClass> listUrl = new ArrayList<>();
        URLClass urls = null;
        Cursor cursorURL = null;

        cursorURL = db.rawQuery(" SELECT * FROM " + DbHelper.TABLE_URL,null);

        if(cursorURL.moveToFirst())
        {
            do {
                urls = new URLClass();
                urls.setPrueba(cursorURL.getInt(0));
                urls.setHola(cursorURL.getString(1));
                urls.setUrl(cursorURL.getString(2));

                listUrl.add(urls);
            } while (cursorURL.moveToNext());
        }

        cursorURL.close();
        return listUrl;
    }

    public MonumentoInfo getInformacion(int id) {

        MonumentoInfo monumentoInfo = new MonumentoInfo();

        Cursor cursorMonInf = null;
        cursorMonInf = db.rawQuery(" SELECT * FROM " + DbHelper.TABLE_URL + " WHERE id=" + id,null);

        if(cursorMonInf.moveToFirst())
        {
            do {
                monumentoInfo.setId(cursorMonInf.getInt(0));
                monumentoInfo.setTitulo(cursorMonInf.getString(1));
                monumentoInfo.setImgUrl(cursorMonInf.getString(2));

            } while (cursorMonInf.moveToNext());
        }

        cursorMonInf.close();


        return monumentoInfo;
    }

    public boolean eliminarInfo (int id)
    {
        boolean confirmacion;

        try {
            db.execSQL("DELETE FROM " + DbHelper.TABLE_URL + " WHERE id=" + id);
            confirmacion = true;
        } catch (Exception ex)
        {
            ex.toString();
            confirmacion = false;
        }
        db.close();

        return confirmacion;
    }

    public boolean editarInfo (int id, String titulo, String url)
    {
        boolean confirmacion;

        try {
            db.execSQL("UPDATE " + DbHelper.TABLE_URL + " SET titulo = '" + titulo + "', url = '" + "' WHERE id='" + id + "' ");
            confirmacion = true;
        } catch (Exception ex)
        {
            ex.toString();
            confirmacion = false;
        } finally {

            db.close();
        }

        return confirmacion;
    }
}
