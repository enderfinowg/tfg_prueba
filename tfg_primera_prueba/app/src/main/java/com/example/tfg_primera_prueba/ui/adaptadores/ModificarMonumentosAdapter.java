package com.example.tfg_primera_prueba.ui.adaptadores;


import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tfg_primera_prueba.R;
import com.example.tfg_primera_prueba.ui.db.URLClass;
import com.example.tfg_primera_prueba.ui.master.masterdelete.EleccionMonumento;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ModificarMonumentosAdapter extends RecyclerView.Adapter<ModificarMonumentosAdapter.ContactoViewHolder> {

    private View root;
    private Activity act;

    ArrayList<URLClass> listadeUrls;

    public ModificarMonumentosAdapter(Activity act, ArrayList<URLClass> listadeUrls)
    {
        this.act = act;
        this.listadeUrls = listadeUrls;
    }

    @NonNull
    @Override
    public ContactoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_urls, null, false);
        return new ContactoViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactoViewHolder holder, int position) {

        holder.textView2.setText(listadeUrls.get(position).getHola());
        String url = (String) listadeUrls.get(position).getUrl();

        ImageView cargarImagen = (ImageView) root.findViewById(R.id.imgURL);
        if (cargarImagen == null) {
            Log.w("ModificarMonumentosAdapter", "imagen null");
        }
        Picasso.get()
                .load(url)
                .error(R.mipmap.logotipo)
                .into(cargarImagen);

        int id = listadeUrls.get(position).getPrueba();

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(act.getApplicationContext(), EleccionMonumento.class);
                intent.putExtra("idMonumento", id);
                act.startActivity(intent);
            }
        };
        cargarImagen.setOnClickListener(listener);

    }

    @Override
    public int getItemCount() {
        return listadeUrls.size();
    }

    public class ContactoViewHolder extends RecyclerView.ViewHolder {

        TextView textView2;

        public ContactoViewHolder(@NonNull View itemView) {
            super(itemView);

            textView2 = itemView.findViewById(R.id.textView2);

        }
    }
}
