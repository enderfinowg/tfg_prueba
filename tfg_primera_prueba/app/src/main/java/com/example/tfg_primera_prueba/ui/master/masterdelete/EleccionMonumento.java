package com.example.tfg_primera_prueba.ui.master.masterdelete;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.tfg_primera_prueba.MainActivity;
import com.example.tfg_primera_prueba.R;
import com.example.tfg_primera_prueba.ui.db.Db;

public class EleccionMonumento extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar);

        Button eliminar, editar, confirmar;
        TextView textNombreEditar, textURLEditar;

        eliminar = (Button) this.findViewById(R.id.eliminarMonumento);
        editar = (Button) this.findViewById(R.id.editarMonumento);
        confirmar = (Button) this.findViewById(R.id.confirmareditarMonumento);
        textNombreEditar = (TextView) this.findViewById(R.id.txtnombreeditarmon);
        textURLEditar = (TextView) this.findViewById(R.id.txturleditarmon);

        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id = getIntent().getExtras().getInt("idMonumento");
                AlertDialog.Builder builder = new AlertDialog.Builder(EleccionMonumento.this);
                builder.setMessage("¿Seguro que se quiere eliminar dicho punto")
                        .setPositiveButton("SI", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i)
                            {
                                Db.getInstance().eliminarInfo(id);
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("NO", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }).show();
            }
        });

        editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                textNombreEditar.setVisibility(View.VISIBLE);
                textURLEditar.setVisibility(View.VISIBLE);
                eliminar.setVisibility(View.INVISIBLE);
                editar.setVisibility(View.INVISIBLE);
                confirmar.setVisibility(View.VISIBLE);
            }
        });

        }
}
