package com.example.tfg_primera_prueba.ui.CrearRuta;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.tfg_primera_prueba.databinding.FragmentCrearRutaBinding;

public class CrearRutaFragment extends Fragment {

    private CrearRutaViewModel crearRutaViewModel;
    private FragmentCrearRutaBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        crearRutaViewModel =
                new ViewModelProvider(this).get(CrearRutaViewModel.class);

        binding = FragmentCrearRutaBinding.inflate(inflater, container, false);
        View root = binding.getRoot();


        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}