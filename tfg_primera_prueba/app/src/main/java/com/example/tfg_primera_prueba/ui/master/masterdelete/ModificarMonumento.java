package com.example.tfg_primera_prueba.ui.master.masterdelete;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tfg_primera_prueba.R;
import com.example.tfg_primera_prueba.ui.adaptadores.ModificarMonumentosAdapter;
import com.example.tfg_primera_prueba.ui.db.Db;
import com.example.tfg_primera_prueba.ui.db.URLClass;

import java.util.ArrayList;

public class ModificarMonumento extends AppCompatActivity {

    RecyclerView listaURLs;
    ArrayList<URLClass> listaArrayURLs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_monument_modificar);


        listaURLs = this.findViewById(R.id.ViElem);
        listaURLs.setLayoutManager(new LinearLayoutManager(this));

        listaArrayURLs = new ArrayList<>();
        ModificarMonumentosAdapter adapter = new ModificarMonumentosAdapter(this, Db.getInstance().mostrarURLs());
        listaURLs.setAdapter(adapter);
    }
}