package com.example.tfg_primera_prueba.ui.Monumentos;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MonumentosViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public MonumentosViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is slideshow fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}