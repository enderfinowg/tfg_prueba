package com.example.tfg_primera_prueba.ui.Comercio;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.tfg_primera_prueba.databinding.FragmentCrearRutaBinding;

public class ComercioFragment extends Fragment {

    private ComercioViewModel comercioViewModel;
    private FragmentCrearRutaBinding binding;



    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        comercioViewModel =
                new ViewModelProvider(this).get(ComercioViewModel.class);

        binding = FragmentCrearRutaBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}