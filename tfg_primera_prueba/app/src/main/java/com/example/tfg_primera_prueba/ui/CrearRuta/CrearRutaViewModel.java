package com.example.tfg_primera_prueba.ui.CrearRuta;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CrearRutaViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public CrearRutaViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is home fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}